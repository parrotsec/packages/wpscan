#!/bin/sh

set -e

# wpscan creates a directory in ~/ and it fails if it can't
mkdir /tmp/fakehome
HOME=/tmp/fakehome
wpscan -h
